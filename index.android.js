import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet
} from 'react-native';
import {Navigator} from 'react-native-deprecated-custom-components';

let navigator;

import {Home} from './app/Component/Home'

class Livescores extends Component {
  render() {
    return (
      <Navigator
        ref={(nav) => { navigator = nav; }}
        style={styles.navigator}
        initialRoute={{id: 'Home'}}
        configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight }
        renderScene={(route, nav) => {return this.renderScene(route, nav);}}
      />
    );
  }

  renderScene = (route, nav) => {
    return <Home navigator={nav} />;
  }
}

const styles = StyleSheet.create({
  navBarText: {
    marginTop: -3,
    fontSize: 16,
    color: '#ffffff',
  },
  navBarTitleText: {
    fontWeight: '500',
  },
  navBarLeftButton: {
    paddingLeft: 10,
  },
  navBarRightButton: {
    paddingRight: 10,
  },
});

AppRegistry.registerComponent('Livescores', () => Livescores);
