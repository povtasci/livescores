export const BASE_URL = 'http://188.166.167.154:16666/api/1.0';

export const ROUTES = {
  HOME_DATA: BASE_URL + '/livescore/today',
  MATCH_DATA: BASE_URL + '/livescore/{id}',
  replaceParams: (URL, params) => {
    for (let i = 0; i < params.length; i++) {
      URL = URL.replace("{" + params[i]['param'] + "}", params[i]['value']);
    }

    return URL;
  }
};
