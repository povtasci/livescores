export const StatusMap = {
  "NS": ["NS", "CANCL", "POSTP", "ABAN"],
  "LIVE": ["LIVE", "HT", "ET", "PEN_LIVE", "BREAK",],
  "FT": ["FT", "INT", "POSTP", "SUSP"],
};