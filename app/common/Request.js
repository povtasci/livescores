import {Platform} from 'react-native';
import Device from 'react-native-device-info';
import {ROUTES} from './Routes';
import {Header} from './Header';

export class Request {
  routeMap = {
    Home: ROUTES.HOME_DATA,
    Match: ROUTES.MATCH_DATA,
  };

  method = 'GET';
  route;
  routeParams = [];
  callback;
  urlParams = {};
  data = {};

  setDefaultParams() {
    const deviceInfo = {
      "m": Device.getModel(),
      "b": Device.getBrand(),
      "s": Device.getSystemName(),
      "s_v": Device.getSystemVersion(),
      "b_n": Device.getBuildNumber(),
      "v": Device.getVersion(),
      "l": Device.getDeviceLocale(),
      "c": Device.getDeviceCountry(),
      "t": Device.getTimezone(),
      "i_t": Device.isTablet(),
    };
    this.setUrlParam('os', Platform.OS);
    this.setUrlParam('d_i', Device.getUniqueID());
    this.setUrlParam('d', JSON.stringify(deviceInfo));


    let date = new Date();
    this.setUrlParam('t', date.getTimezoneOffset() / 60);
  }

  send() {
    this.setDefaultParams();

    fetch(this.getUrl(),
      {
        method: this.method,
        headers: Header,
        data: this.data
      })
      .then((response) => {
        return response.json()
      })
      .then(this.callback)
      .done();
  }

  getUrl() {
    let url = this.routeMap[this.route];

    if (this.routeParams.length > 0) {
      url = ROUTES.replaceParams(url, this.routeParams);
    }

    url += this.getUrlParams();
    return url;
  }

  setUrlParam(key, value) {
    this.urlParams[key] = value;
  }

  getUrlParams() {
    let str = "";
    for (let key in this.urlParams) {
      if (str !== "") {
        str += "&";
      } else {
        str += "?";
      }
      str += key + "=" + encodeURIComponent(this.urlParams[key]);

    }

    return str;
  }

  setMethod(method = 'GET') {
    this.method = method;
  }

  setRoute(route, routeParams = []) {
    this.route = route;
    this.routeParams = routeParams;
  }

  setData(data) {
    this.data = data;
  }

  setCallBack(callback) {
    this.callback = callback;
  }
}
