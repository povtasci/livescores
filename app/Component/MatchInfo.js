import React, {Component} from 'react';
import {
  View,
  NetInfo,
  Image,
  StyleSheet,
  Text,
  ListView,
  ActivityIndicator,
  Linking,
  StatusBar,
  Platform,
  ScrollView,
  Dimensions,
  TouchableOpacity
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';
import DefaultTabBar from 'react-native-scrollable-tab-view/DefaultTabBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import {Request} from '../common/Request';
import {StatusMap} from '../common/Constants';


export class MatchInfo extends Component {
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      loading: true,
      match: props.match,
      home_team_lineup: [],
      away_team_lineup: [],
      home_team_bench: [],
      away_team_bench: [],
    };
  }

  componentDidMount() {
    this.getMatchInfo();
  }

  render() {
    return (
      <Image source={require('../img/bg.png')} style={styles.mainView} resizeMode="stretch">
        <View style={styles.header}>
          <TouchableOpacity onPress={() => this.props.setModalVisible(false)} style={{flex: 3}}>
            <Text style={[styles.title, styles.blueText]}>Back</Text>
          </TouchableOpacity>
          <View style={{flex: 6}}>
          </View>
          <View style={{flex: 3, paddingRight: 5, alignItems: 'center'}}>
          </View>
        </View>
        <View style={styles.matchInfo}>
          <View style={{flex: 4, alignItems: "center"}}>
            {this.getImagesPart.call(this, 'home_team')}
            <Text style={styles.team_name}>{this.state.match.home_team.name}</Text>
          </View>
          {this.getScore()}
          <View style={{flex: 4, alignItems: "center"}}>
            {this.getImagesPart.call(this, 'away_team')}
            <Text style={styles.team_name}>{this.state.match.away_team.name}</Text>
          </View>
        </View>
        {this.getTime()}
        <ScrollableTabView
          tabBarBackgroundColor="transparent"
          tabBarActiveTextColor="#FFFFFF"
          tabBarInactiveTextColor="#aac0d9"
          tabBarUnderlineStyle={{backgroundColor: '#aac0d9'}}
          tabBarTextStyle={{fontSize: 14}}
          renderTabBar={() => <DefaultTabBar
            style={{height: 45, borderBottomColor: '#aac0d9', borderTopColor: '#aac0d9'}}/>}
          activeTab={1}
        >
          <ScrollView style={styles.lineupsTab} tabLabel="Lineups">
            {this.getLineups()}
          </ScrollView>
          <ScrollView style={styles.lineupsTab} tabLabel="Activities">
            {this.getLineups()}
          </ScrollView>
        </ScrollableTabView>
      </Image>
    );
  }

  getTime = () => {
    let text;
    if (StatusMap.LIVE.indexOf(this.state.match.status) === -1) {
      text = this.getFormattedDateForPayment(this.state.match.starting_date, this.state.match.starting_time);
    } else if (this.state.match.status === "HT") {
      text = "Half Time";
    } else if (this.state.match.status === "PEN_LIVE") {
      text = "Penalties";
    } else if (this.state.match.status === "BREAK") {
      text = "Break";
    } else {
      text = `${this.state.match.minute}"`;
    }

    return (
      <View style={styles.matchDateBlock}>
        <Text style={[styles.matchDate, styles.blueText]}>
          {text}
        </Text>
      </View>
    )
  }

  getPlayerEvents = (player) => {
    let events = [];

    if (parseInt(player.stats.goals.scored) > 0) {
      for (let i = 0; i < parseInt(player.stats.goals.scored); i++) {
        events.push(<Image key={`goal-${i}`} style={styles.eventItemImage}
                           source={require('../img/game_events/goal.png')}/>);
      }
    }

    if (parseInt(player.stats.cards.yellowcards) > 0) {
      events.push(<Image key={`yellow-1`} style={styles.eventItemImage}
                         source={require('../img/game_events/yellow.png')}/>);
      if (parseInt(player.stats.cards.yellowcards) > 1) {
        events.push(<Image key={`yellow-2`} style={styles.eventItemImage}
                           source={require('../img/game_events/y2c.png')}/>);
      }
    }

    if (parseInt(player.stats.cards.redcards) > 0) {
      events.push(<Image key={`red`} style={styles.eventItemImage}
                         source={require('../img/game_events/red.png')}/>);
    }

    return events;
  }

  getLineups = () => {
    if (this.state.loading) {
      return <Spinner visible={this.state.loading}/>;
    }
    if (this.state.home_team_lineup.length === 0) {
      return (
        <View>
          <Text style={[styles.loadingText, styles.blueText]}>No lineups yet</Text>
        </View>
      );
    }
    return (
      <View>
        <View style={styles.lineupsBlock}>
          <View style={[styles.lineupsColumn, styles.leftLineupsColumn]}>
            {this.state.home_team_lineup.map((player) => {
              return (
                <View key={`player-${player.player_id}`} style={styles.lineupsPlayer}>
                  <Text style={styles.lineupsPlayerNumber}>{player.number}.</Text>
                  <Text style={styles.lineupsPlayerName}>{player.player_name}</Text>
                  {this.getPlayerEvents(player)}
                </View>
              );
            })}
          </View>
          <View style={styles.lineupsColumn}>
            {this.state.away_team_lineup.map((player) => {
              return (
                <View key={`player-${player.player_id}`} style={styles.lineupsPlayer}>
                  <Text style={styles.lineupsPlayerNumber}>{player.number}.</Text>
                  <Text style={styles.lineupsPlayerName}>{player.player_name}</Text>
                  {this.getPlayerEvents(player)}
                </View>
              );
            })}
          </View>
        </View>
        <View style={styles.benchTitleBlock}>
          <Text style={styles.benchTitle}>Bench</Text>
        </View>
        <View style={styles.lineupsBlock}>
          <View style={[styles.lineupsColumn, styles.leftLineupsColumn]}>
            {this.state.home_team_bench.map((player) => {
              return (
                <View key={`player-${player.player_id}`} style={styles.lineupsPlayer}>
                  <Text style={styles.lineupsPlayerNumber}>{player.number}.</Text>
                  <Text style={styles.lineupsPlayerName}>{player.player_name}</Text>
                  {this.getPlayerEvents(player)}
                </View>
              );
            })}
          </View>
          <View style={styles.lineupsColumn}>
            {this.state.away_team_bench.map((player) => {
              return (
                <View key={`player-${player.player_id}`} style={styles.lineupsPlayer}>
                  <Text style={styles.lineupsPlayerNumber}>{player.number}.</Text>
                  <Text style={styles.lineupsPlayerName}>{player.player_name}</Text>
                  {this.getPlayerEvents(player)}
                </View>
              );
            })}
          </View>
        </View>
      </View>
    )
  }

  getScore = () => {
    if (StatusMap.NS.indexOf(this.state.match.status) === -1) {
      return (
        <View style={{flex: 2, alignItems: 'center', flexDirection: 'row'}}>
          <View style={styles.scoreBlock}>
            <Text style={styles.score}>{this.state.match.home_score}</Text>
          </View>
          <View style={{width: 20, alignItems: 'center'}}>
            <Text style={styles.scoreDots}>:</Text>
          </View>
          <View style={styles.scoreBlock}>
            <Text style={styles.score}>{this.state.match.away_score}</Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={{flex: 2, alignItems: "center", flexDirection: 'row'}}>
          <View style={styles.scoreBlock}>
            <Text style={styles.score}>-</Text>
          </View>
          <View style={{width: 20, alignItems: 'center'}}>
            <Text style={styles.scoreDots}>:</Text>
          </View>
          <View style={styles.scoreBlock}>
            <Text style={styles.score}>-</Text>
          </View>
        </View>
      );
    }
  }

  formatTime = (time) => {
    let time_list = time.split(':');
    return time_list[0] + ':' + time_list[1];
  }

  getImagesPart = (team_number) => {
    if (!this.state.match) {
      return false;
    }
    if (this.state.match[team_number].logo_path) {
      const team = this.state.match[team_number];
      return (
        <Image style={{width: 50, height: 50}} source={{uri: team.logo_path}}
               defaultSource={require('../img/default.png')}/>
      );
    } else {
      if (this.state.match.home_score === this.state.match.away_score) {
        return (
          <Image style={{width: 50, height: 50}} source={require('../img/dot_gray.png')}/>
        );
      } else {
        if (
          this.state.match.home_score > this.state.match.away_score && team_number === "home_team" ||
          this.state.match.home_score < this.state.match.away_score && team_number === "away_team"
        ) {
          return (
            <Image style={{width: 50, height: 50}} source={require('../img/dot_green.png')}/>
          );
        } else {
          return (
            <Image style={{width: 50, height: 50}} source={require('../img/dot_red.png')}/>
          );
        }
      }
    }
  }

  getMatchInfo = () => {
    const self = this;
    const requester = new Request();

    requester.setRoute('Match', [{param: 'id', value: this.state.match.id}]);
    requester.setMethod('GET');
    requester.setCallBack((response) => {
      let home_team_lineup = [];
      let away_team_lineup = [];
      let home_team_bench = [];
      let away_team_bench = [];

      if (response.lineup) {
        for (let i = 0; i < response.lineup.data.length; i++) {
          if (response.lineup.data[i].team_id === response.home_team.id) {
            home_team_lineup.push(response.lineup.data[i]);
          } else {
            away_team_lineup.push(response.lineup.data[i]);
          }
        }
      }

      if (response.bench) {
        for (let i = 0; i < response.bench.data.length; i++) {
          if (response.bench.data[i].team_id === response.home_team.id) {
            home_team_bench.push(response.bench.data[i]);
          } else {
            away_team_bench.push(response.bench.data[i]);
          }
        }
      }

      self.setState({
        match: response,
        loading: false,
        home_team_lineup: home_team_lineup,
        away_team_lineup: away_team_lineup,
        home_team_bench: home_team_bench,
        away_team_bench: away_team_bench,
      });
    });
    requester.send();
  }

  getFormattedDateForPayment = (date, time) => {
    if (!date) {
      return "";
    }

    let monthNames = [
      "Jan", "Feb", "Mar", "Apr", "May", "Jun",
      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];

    let dateArray = date.split("-");
    let timeArray = time.split(":");
    return monthNames[parseInt(dateArray[1], 10) - 1] + ' ' + dateArray[2] + ', ' + timeArray[0] + ':' + timeArray[1];
  };

}

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: '#797d89',
    flex: 1,
    flexDirection: 'column',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height - StatusBar.currentHeight,
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null
  },
  header: {
    height: (Platform.OS == 'ios' ? 62 : 50),
    flexDirection: 'row',
    paddingTop: (Platform.OS == 'ios' ? 10 : 0),
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'Lato-Bold'
  },
  team_name: {
    fontSize: 18,
    color: '#ffffff',
    fontFamily: 'Lato-Bold',
    textAlign: 'center',
  },
  scoreBlock: {
    borderRadius: 3,
    width: 30,
    overflow: 'hidden',
  },
  score: {
    backgroundColor: '#ffffff',
    color: '#34495e',
    paddingVertical: 2,
    width: 30,
    fontSize: 40,
    borderRadius: 3,
    textAlign: 'center',
    fontFamily: 'bigNoodleTitling',
  },
  scoreDots: {
    color: '#ffffff',
    paddingVertical: 2,
    width: 20,
    fontSize: 30,
    textAlign: 'center',
    fontFamily: 'bigNoodleTitling',
  },
  blueText: {
    color: '#aac0d9'
  },
  fontLB: {
    fontFamily: 'Lato-Bold'
  },
  fontLH: {
    fontFamily: 'Lato-Heavy'
  },
  fontBNT: {
    fontFamily: 'bigNoodleTitling',
  },
  teamHomeRow: {
    flexDirection: 'row',
    height: 42,
    paddingRight: 7,
    paddingLeft: 4,
    borderBottomWidth: 0.5,
    borderBottomColor: '#b7b7b7',
    alignItems: 'center',
  },
  teamAwayRow: {
    paddingRight: 7,
    paddingLeft: 4,
    flexDirection: 'row',
    height: 42,
    alignItems: 'center',
  },
  listView: {
    paddingTop: 8.5
  },
  // Match Info
  matchInfo: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lineupsBlock: {
    flexDirection: 'row',
  },
  lineupsColumn: {
    flex: 1,
  },
  leftLineupsColumn: {
    borderRightWidth: 0.5,
    borderRightColor: '#b7b7b7',
  },
  lineupsPlayer: {
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    padding: 5,
  },
  lineupsPlayerNumber: {
    width: 25,
    fontFamily: 'Lato-Bold',
    color: '#000000',
  },
  lineupsPlayerName: {
    fontFamily: 'Lato-Bold',
    color: '#000000',
  },
  benchTitleBlock: {
    marginVertical: 3,
    paddingVertical: 3,
    backgroundColor: '#ffffff',
    alignItems: 'center'
  },
  benchTitle: {
    fontSize: 18,
    color: '#000000',
    fontFamily: 'Lato-Bold',
  },
  eventItemImage: {
    width: 13,
    height: 13,
    marginHorizontal: 3,
    marginTop: 4,
  },
  loadingText: {
    marginTop: 30,
    fontSize: 15,
    textAlign: 'center',
    fontFamily: 'Lato-Bold',
  },
  matchDateBlock: {
    paddingBottom: 20,
  },
  matchDate: {
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Lato-Bold',
  },
});