import React, {Component} from 'react';
import {
  View,
  NetInfo,
  Image,
  StyleSheet,
  Text,
  ListView,
  Linking,
  StatusBar,
  Platform,
  Dimensions,
  TouchableOpacity
} from 'react-native';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import DefaultTabBar from 'react-native-scrollable-tab-view/DefaultTabBar';
import Spinner from 'react-native-loading-spinner-overlay';

import {Request} from '../common/Request';
import {MatchCard} from "./MatchCard";
import {StatusMap} from "../common/Constants";
import {ChampionshipCard} from "./ChampionshipCard";

export class Home extends Component {
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      matches: this.ds.cloneWithRows([]),
      finished_matches: this.ds.cloneWithRows([]),
      loading: true,
      show_more_loading: false,
      statistic: false,
      finished_matches_data: [],
      show_images: false,
      modal_visible: false,
      loading_match_info: false,
      page: 1,
    };
  }

  componentDidMount() {
    this.checkNetwork();

    setInterval(this.getUpcomingMatches.bind(this), 10000);
  }

  checkNetwork() {
    let self = this;
    NetInfo.isConnected.fetch().then().done(
      NetInfo.isConnected.addEventListener('connectionChange', (isConnected) => {
        self.setState({
          offline: !isConnected
        });

        if (isConnected) {
          self.getData.bind(self)();
        } else {
          self.setState({
            loading: false,
          });
        }
      })
    );
  }

  getData() {
    this.getUpcomingMatches();
  }

  render() {
    let content;
    if (!this.state.loading) {
      content = (
        <ScrollableTabView
          tabBarBackgroundColor="transparent"
          tabBarActiveTextColor="#FFFFFF"
          tabBarInactiveTextColor="#aac0d9"
          tabBarUnderlineStyle={{backgroundColor: '#aac0d9'}}
          tabBarTextStyle={{fontSize: 14}}
          renderTabBar={() => <DefaultTabBar style={{height: 45, borderBottomColor: '#aac0d9'}}/>}
          activeTab={1}
        >
          <View tabLabel="Today">
            <ListView
              style={styles.listView}
              dataSource={this.state.championships}
              enableEmptySections={true}
              renderFooter={() => <View style={{height: 8.5}}/>}
              renderRow={(championship, sectionId, rowId) => {
                return (
                  <View>
                    <ChampionshipCard championship={championship}/>
                    {championship.matches.map((match) => <MatchCard key={`match-${match.id}`} match={match}/>)}
                  </View>
                );
              }}/>
          </View>
          <View tabLabel="Live">
            <ListView
              style={styles.listView}
              dataSource={this.state.championships}
              enableEmptySections={true}
              renderFooter={() => <View style={{height: 8.5}}/>}
              renderRow={(championship, sectionId, rowId) => {
                let show_championship = false;
                for (let i = 0; i < championship.matches.length; i++) {
                  if (StatusMap.LIVE.indexOf(championship.matches[i].status) > -1) {
                    show_championship = true;
                    break;
                  }
                }

                if (!show_championship) {
                  return null;
                }

                return (
                  <View>
                    <ChampionshipCard championship={championship}/>
                    {championship.matches.map((match) => {
                      if (StatusMap.LIVE.indexOf(match.status) !== -1) {
                        return <MatchCard key={`match-${match.id}`} match={match}/>;
                      }
                      return null;
                    })}
                  </View>
                );
              }}/>
          </View>
        </ScrollableTabView>
      );
    }

    return (
      <Image source={require('../img/bg.png')} style={styles.mainView} resizeMode="stretch">
        <Spinner visible={this.state.loading}/>

        <View style={styles.header}>
          <View style={{flex: 3}}>
          </View>
          <View style={{flex: 6}}>
            <Text style={[styles.title, styles.blueText]}>LiveScore</Text>
          </View>
          <View style={{flex: 3, paddingRight: 5, alignItems: 'center'}}>
            <TouchableOpacity onPress={() => {
              Alert.alasd9();
              Linking.openURL('https://www.dictoro.com');
            }}>
              <Text style={[{fontSize: 8, textAlign: 'center', fontFamily: 'Lato-Bold'}, styles.blueText]}>powered
                by</Text>
              <Text
                style={[{fontSize: 11.5, textAlign: 'center', fontFamily: 'Lato-Bold'}, styles.blueText]}>Dictoro</Text>
            </TouchableOpacity>
          </View>
        </View>
        {content}
      </Image>
    );
  }

  getUpcomingMatches = () => {
    const self = this;
    const requester = new Request();

    requester.setRoute('Home');
    requester.setMethod('GET');
    requester.setCallBack((response) => {
      self.setState({
        championships: self.ds.cloneWithRows(response),
        loading: false
      });
    });
    requester.send();
  }

  getMatchInfo = (id) => {
    const self = this;
    const requester = new Request();

    requester.setRoute('Match', [{param: 'id', value: id}]);
    requester.setMethod('GET');
    requester.setCallBack((response) => {
      self.setState({
        match_info: response,
        loading_match_info: false
      });
    });
    requester.send();
  }

}

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: '#797d89',
    flex: 1,
    flexDirection: 'column',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height - StatusBar.currentHeight,
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null
  },
  header: {
    height: (Platform.OS == 'ios' ? 62 : 50),
    flexDirection: 'row',
    paddingTop: (Platform.OS == 'ios' ? 10 : 0),
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 22,
    textAlign: 'center',
    fontFamily: 'Lato-Bold'
  },
  blueText: {
    color: '#aac0d9'
  },
  fontLB: {
    fontFamily: 'Lato-Bold'
  },
  fontLH: {
    fontFamily: 'Lato-Heavy'
  },
  fontBNT: {
    fontFamily: 'bigNoodleTitling',
  },
  listView: {
    paddingTop: 8.5
  },
});