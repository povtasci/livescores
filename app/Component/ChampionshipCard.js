import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
} from 'react-native';

export class ChampionshipCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.championship_line}>
        <Image style={styles.championship_icon} source={{uri: this.props.championship.flag}}
               defaultSource={require('../img/default.png')}/>
        <Text style={styles.championship_name}>{this.props.championship.name}</Text>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  championship_line: {
    flexDirection: 'row',
    overflow: 'hidden',
    marginVertical: 1.5,
    marginHorizontal: 10,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
  },
  championship_icon: {
    width: 30,
    height: 20,
    marginTop: 8,
    marginLeft: 8,
    marginBottom: 8,
  },
  championship_name: {
    marginLeft: 8,
    fontSize: 18,
    flex: 1,
    paddingTop: 5,
    paddingBottom: 5,
    color: '#000000',
    fontFamily: 'Lato-Bold',
  },
});