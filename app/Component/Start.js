import React, { Component } from 'react';
import {
    View,
    NetInfo,
    Image,
    StyleSheet,
    Text
} from 'react-native';

import {Request} from '../common/Request';

export class Start extends Component {
    constructor(props) {
        super(props);

        this.handleToken();

        this.state = {
            loading: true,
            offline: false,
        };
    }

    handleToken() {
        let self = this;
        NetInfo.isConnected.fetch().then().done(
            NetInfo.isConnected.addEventListener('change', (isConnected) => {
                self.setState({
                    loading: false,
                    offline: !isConnected
                });

                if (isConnected) {
                    self.props.navigator.replace({id: 'Home'});
                }
            })
        );
    }

    render() {
        let text = <Text style={styles.loadingText}>Loading...</Text>;
        if (!this.state.loading && this.state.offline) {
            text = <Text style={styles.loadingText}>Please check your network!</Text>;
        }
        return (
            <Image source={require('../../img/bg.jpg')} style={styles.backgroundImage}>
                <View style={styles.mainView}>
                    {text}
                </View>
            </Image>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null
    },
    loadingText: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#dddddd',
        textAlign: 'center',
    },
});