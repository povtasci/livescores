import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';

import {StatusMap} from '../common/Constants';
import {MatchInfo} from "./MatchInfo";

export class MatchCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modal_visible: false,
    };
  }

  render() {
    return (
      <TouchableWithoutFeedback
        onPress={this.setModalVisible.bind(this, true)}
      >
        <View style={styles.match_line}>
          <View style={[styles.date_block, this.getTimeBlockStyle()]}>
            <View style={[styles.date_block2, this.getTimeBlockStyle()]}>
              {this.getTime("away_score")}
            </View>
          </View>
          <View style={styles.teams_block}>
            <View style={styles.teamHomeRow}>
              {this.getImagesPart('home_team')}
              <Text style={styles.team_name}>{this.props.match.home_team.name}</Text>
              {this.getScore("home_score")}
            </View>
            <View style={styles.teamAwayRow}>
              {this.getImagesPart('away_team')}
              <Text style={styles.team_name}>{this.props.match.away_team.name}</Text>
              {this.getScore("away_score")}
            </View>
          </View>
          <Modal
            animationType={"slide"}
            transparent={false}
            visible={this.state.modal_visible}
            onRequestClose={() => {
            }}
          >
            <MatchInfo
              match={this.props.match}
              setModalVisible={this.setModalVisible.bind(this)}
            />
          </Modal>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  setModalVisible = (visible) => {
    this.setState({
      modal_visible: visible,
    });
  }

  getTimeBlockStyle = () => {
    return {
      backgroundColor: (StatusMap.LIVE.indexOf(this.props.match.status) === -1 ? '#ffffff' : '#50af31'),
    };
  }

  getTimeTextStyle = () => {
    return {
      color: (StatusMap.LIVE.indexOf(this.props.match.status) === -1 ? '#34495e' : '#ffffff'),
    };
  }

  getScore = (score) => {
    if (StatusMap.NS.indexOf(this.props.match.status) === -1) {
      return (
        <View style={styles.scoreBlock}>
          <Text style={styles.score}>{this.props.match[score]}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  getTime = () => {
    let style = [styles.dateText, this.getTimeTextStyle()];
    if (StatusMap.LIVE.indexOf(this.props.match.status) === -1) {
      return (
        <Text style={style}>{this.formatTime(this.props.match.starting_time)}</Text>
      );
    } else if (this.props.match.status === "HT") {
      return (
        <Text style={style}>HT</Text>
      );
    } else if (this.props.match.status === "PEN_LIVE") {
      return (
        <Text style={style}>PEN</Text>
      );
    } else if (this.props.match.status === "BREAK") {
      return (
        <Text style={style}>BRK</Text>
      );
    } else {
      return (
        <Text style={style}>{this.props.match.minute}"</Text>
      );
    }
  }

  formatTime = (time) => {
    let time_list = time.split(':');
    return time_list[0] + ':' + time_list[1];
  }

  getImagesPart = (team_number) => {
    if (this.props.match[team_number].logo_path) {
      const team = this.props.match[team_number];
      return (
        <Image style={{width: 20, height: 20, marginHorizontal: 3}} source={{uri: team.logo_path}}
               defaultSource={require('../img/default.png')}/>
      );
    } else {
      if (this.props.match.home_score === this.props.match.away_score) {
        return (
          <Image style={{width: 8, height: 8, marginHorizontal: 8}} source={require('../img/dot_gray.png')}/>
        );
      } else {
        if (this.props.match.home_score > this.props.match.away_score) {
          return (
            <Image style={{width: 8, height: 8, marginHorizontal: 8}} source={require('../img/dot_green.png')}/>
          );
        } else {
          return (
            <Image style={{width: 8, height: 8, marginHorizontal: 8}} source={require('../img/dot_red.png')}/>
          );
        }
      }
    }
  }
}

const styles = StyleSheet.create({
  match_line: {
    overflow: 'hidden',
    flexDirection: 'row',
    marginVertical: 1.5,
    marginHorizontal: 10,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
  },
  teams_block: {
    flex: 7,
    borderLeftWidth: 0.5,
    borderLeftColor: '#b7b7b7',
  },
  date_block: {
    width: 50,
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  date_block2: {
    flex: 1,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  dateText: {
    fontSize: 20,
    fontFamily: 'bigNoodleTitling',
    color: "#34495e",
  },
  team_name: {
    fontSize: 18,
    flex: 1,
    color: '#000000',
    fontFamily: 'Lato-Bold',
  },
  scoreBlock: {
    borderRadius: 3,
    width: 20,
    overflow: 'hidden',
  },
  score: {
    backgroundColor: '#34495e',
    color: '#ffffff',
    paddingVertical: 2,
    width: 20,
    fontSize: 24,
    borderRadius: 3,
    textAlign: 'center',
    fontFamily: 'bigNoodleTitling',
  },
  teamHomeRow: {
    flexDirection: 'row',
    height: 42,
    paddingRight: 7,
    paddingLeft: 4,
    borderBottomWidth: 0.5,
    borderBottomColor: '#b7b7b7',
    alignItems: 'center',
  },
  teamAwayRow: {
    paddingRight: 7,
    paddingLeft: 4,
    flexDirection: 'row',
    height: 42,
    alignItems: 'center',
  },
});